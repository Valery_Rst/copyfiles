package net.rstvvoli.fileCopying;

/**
 * Синхронное (последовательное) копирование двух файлов;
 */
public class SyncCopy {
    public static void main(String[] args) {

        PatternForCopy copyFile1 = new PatternForCopy(EnterPath.enterPathToOriginal(), EnterPath.enterPathToOriginal());
        PatternForCopy copyFile2 = new PatternForCopy(EnterPath.enterPathToOriginal(), EnterPath.enterPathToOriginal());

        System.out.println(copyFile1);
        System.out.println(copyFile2);

        long before = System.currentTimeMillis();

        copyFile1.copy();
        copyFile2.copy();

        long after = System.currentTimeMillis();
        System.out.printf("Примерное время выполнения кода: %d (ms)", (after - before));
    }
}