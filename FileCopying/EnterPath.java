package net.rstvvoli.fileCopying;

import java.util.Scanner;

class EnterPath {

    private static Scanner scanner = new Scanner(System.in);

    /**
     *Метод предназначен для возвращения расположения исходного файла;
     * @return путь к исходному файлу;
     */
    static String enterPathToOriginal() {

        System.out.println("Укажите расположение исходного файла. \n" +
                "Например: E:\\forproject\\original.txt \n");
        return scanner.nextLine();
    }

    /**
     * Метод предназначен для возвращения расположения будущей копии;
     * @return путь к будущей копии:
     */
    static String enterPathToCopy() {
        System.out.println("Укажите расположение будущей копии. \n" +
                "Например: E:\\forproject\\copy.txt \n");
        return scanner.nextLine();
    }
}