package net.rstvvoli.fileCopying;

/**
 * Асинхронное (параллельное) копирование двух файлов;
 */
public class AsyncCopy  {
    public static void main(String[] args) throws InterruptedException {
        PatternForCopy copyFile1 = new PatternForCopy(EnterPath.enterPathToOriginal(), EnterPath.enterPathToCopy());
        PatternForCopy copyFile2 = new PatternForCopy(EnterPath.enterPathToOriginal(), EnterPath.enterPathToCopy());

        long before = System.currentTimeMillis();

        Thread thread1 = new Thread(() -> copyFile1.copy());
        thread1.start();

        Thread thread2 = new Thread(() -> copyFile2.copy());
        thread2.start();

        thread1.join();
        thread2.join();

        long after = System.currentTimeMillis();
        System.out.printf("Примерное время выполнения кода: %d (ms)", (after - before));
    }
}