package net.rstvvoli.fileCopying;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

class PatternForCopy {
    private static final String ERROR = "Непредвиденная ошибка. !";

    private String original; //Исходный файл;
    private String copy; //Будущая копия;

    /**
     * Возвращает значение поля original;
     * @return исходный файл(оригинал);
     */
    public String getOriginal() {
        return original;
    }

    /**
     * Возвращает значение поля copy;
     * @return будущая копия;
     */
    public String getCopy() {
        return copy;
    }

    /**
     * Инициализирует объект полученными путями к исходному файлу
     * и к файлу копии;
     * @param original путь к оригиналу;
     * @param copy путь к будущей копии;
     */
    PatternForCopy(String original, String copy) {
        this.original = original;
        this.copy = copy;
    }

    /**
     * Метод выполняет копирование оригинального файла в будующую копию файла;
     */
    void copy() {
        try (FileInputStream input = new FileInputStream(original);
             FileOutputStream output = new FileOutputStream(copy)) {

            int text;
            while ((text = input.read()) != -1) {
                output.write(text);
            }
            System.out.println("Файл " + getOriginal() + " скопирован. Копия: " + getCopy() + ";");
        } catch (IOException e) {
            System.out.println(ERROR);
        }
    }
}